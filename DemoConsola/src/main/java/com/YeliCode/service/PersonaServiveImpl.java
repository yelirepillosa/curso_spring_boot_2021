package com.YeliCode.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.YeliCode.repository.IPersona;

@Service
public class PersonaServiveImpl implements IPersonaService {
	
	@Autowired
	@Qualifier("persona1")
	IPersona repo;
	@Override
	public void registrarHandler(String pNombre) {
		//repo = new PersonaRepoImpl();
		repo.registrar(pNombre);
	}

}
