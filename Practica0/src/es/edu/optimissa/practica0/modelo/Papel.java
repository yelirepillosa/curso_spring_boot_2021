package es.edu.optimissa.practica0.modelo;

public class Papel extends PiedraPapelTijeraFactory{
	public Papel() {
		this("Papel", PAPEL);
	}
	public Papel(String pNom, int PNum) {
		super(pNom, PNum);
	}
	
	@Override
	public boolean isMe(int pNum) {
		return pNum == PAPEL;
	}
	
	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		case PIEDRA:
		case SPOCK:	
			resul=1;
			this.descripcionResultado = "papel le gana a " + pPiedPapelTijera.getNombre();
			break;
			
        case TIJERA:
        case LAGARTO:
			resul=-1;
			this.descripcionResultado = "papel pierde contra " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul=0;
			this.descripcionResultado = "papel empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}
}
