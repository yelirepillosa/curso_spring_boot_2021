package es.edu.optimissa.practica0.modelo.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.edu.optimissa.practica0.modelo.Lagarto;
import es.edu.optimissa.practica0.modelo.Papel;
import es.edu.optimissa.practica0.modelo.Piedra;
import es.edu.optimissa.practica0.modelo.PiedraPapelTijeraFactory;
import es.edu.optimissa.practica0.modelo.Spock;
import es.edu.optimissa.practica0.modelo.Tijera;

class PierdraPapelTijeraFactoryTest {

	//1- lote pruebas
		PiedraPapelTijeraFactory piedra, papel, tijera, lagarto, spock;
		
		
	
	@BeforeEach
	void setUp() throws Exception {
		//se ejecuta antes de cada prueba
				piedra = new Piedra();
				papel = new Papel();
				tijera = new Tijera();
				
				lagarto = new Lagarto();
				spock 	= new Spock();
	}

	@AfterEach
	void tearDown() throws Exception {
		//se ejecuta despues de cada prueba
		piedra = null;
		papel  = null;
		tijera = null;
		
		lagarto = null;
		spock 	= null;
	}

	@Test
	void testGetInstancePiedra() {
		assertEquals("piedra", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PIEDRA)
													  .getNombre()
													  .toLowerCase());
		
	}
	@Test
	void testGetInstancePapel() {
		assertEquals("papel", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PAPEL)
													  .getNombre()
													  .toLowerCase());		
	}

	@Test
	void testGetInstanceTiera() {
		assertEquals("tijera", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.TIJERA)
													  .getNombre()
													  .toLowerCase());		
	}
	@Test
	void testGetInstanceLagarto() {
		assertEquals("lagarto", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.LAGARTO)
													  .getNombre()
													  .toLowerCase());		
	}
	@Test
	void testGetInstanceSpock() {
		assertEquals("spock", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.SPOCK)
													  .getNombre()
													  .toLowerCase());		
	}
	
	 //Casos de PIEDRA
	
	@Test
	void testCompararPiedraGanaATijera() {
		//TODO para mis queridos alumnos testCompararPiedraGanaATijera
		assertEquals(1, piedra.comparar(tijera));
		assertEquals("piedra le gana a tijera", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararPiedraGanaALagarto() {
		//TODO para mis queridos alumnos CompararPiedraEmpataConPiedra, agregar el texto del empate en todos loados
		assertEquals(1, piedra.comparar(lagarto));
		assertEquals("piedra le gana a lagarto", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararPiedraPierdeConPapel() {
		assertEquals(-1, piedra.comparar(papel));
		assertEquals("piedra pierde contra papel", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararPiedraPierdeConSpock() {
		//TODO para mis queridos alumnos CompararPiedraEmpataConPiedra, agregar el texto del empate en todos loados
		assertEquals(-1, piedra.comparar(spock));
		assertEquals("piedra pierde contra spock", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPiedraEmpataConPiedra() {
		//TODO para mis queridos alumnos CompararPiedraEmpataConPiedra, agregar el texto del empate en todos loados
		assertEquals(0, piedra.comparar(piedra));
		assertEquals("piedra empata con piedra", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	
	//Casos de PAPEL
	@Test
	void testCompararPapelGanaConPiedra() {
		//TODO para mis queridos alumnos testCompararPapelGanaConPiedra
		assertEquals(1, papel.comparar(piedra));
		assertEquals("papel le gana a piedra", papel.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testPapelGanaSpock() {
		assertEquals(1, papel.comparar(spock));
		assertEquals("papel le gana a spock", papel.getDescripcionResultado()
				  							 	.toLowerCase());
	}
	@Test
	void testCompararPapelPierdeConTijera() {
		//TODO para mis queridos alumnos testCompararPapelPierdeConTijera
		assertEquals(-1, papel.comparar(tijera));
		assertEquals("papel pierde contra tijera", papel.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testPapelPierdeConLagarto() {
		assertEquals(-1, papel.comparar(lagarto));
		assertEquals("papel pierde contra lagarto", papel.getDescripcionResultado()
				  							 	.toLowerCase());
	}
	@Test
	void testCompararPapelEmpataConPapel() {
		//TODO para mis queridos alumnos testCompararPapelEmpataConPapel
		assertEquals(0, papel.comparar(papel));
		assertEquals("papel empata con papel", papel.getDescripcionResultado()
													  .toLowerCase());
	}
	

	//Casos de TIJERA
	@Test
	void testCompararTijeraGanaAPapel() {
		//TODO para mis queridos alumnos testCompararTijeraGanaAPapel() 
		assertEquals(1, tijera.comparar(papel));
		assertEquals("tijera le gana a papel", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararTijeraGanaALagarto() {
		//TODO para mis queridos alumnos testCompararTijeraGanaAPapel() 
		assertEquals(1, tijera.comparar(lagarto));
		assertEquals("tijera le gana a lagarto", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararTijeraPierdeConPiedra() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, tijera.comparar(piedra));
		assertEquals("tijera pierde contra piedra", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararTijeraPierdeConSpock() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, tijera.comparar(spock));
		assertEquals("tijera pierde contra spock", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararTijeraEmpataConTijera() {
		//TODO para mis queridos alumnos testCompararTijeraEmpataConTiera 
		assertEquals(0, tijera.comparar(tijera));
		assertEquals("tijera empata con tijera", tijera.getDescripcionResultado()
													  .toLowerCase());
	}	
	
	//Casos de LAGARTO
	@Test
	void testCompararLagartoGanaASpock() {
		//TODO para mis queridos alumnos testCompararTijeraGanaAPapel() 
		assertEquals(1, lagarto.comparar(spock));
		assertEquals("lagarto le gana a spock", lagarto.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararLagartoGanaAPapel() {
		//TODO para mis queridos alumnos testCompararTijeraGanaAPapel() 
		assertEquals(1, lagarto.comparar(papel));
		assertEquals("lagarto le gana a papel", lagarto.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararLagartoPierdeConTijera() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, lagarto.comparar(tijera));
		assertEquals("lagarto pierde contra tijera", lagarto.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararLagartoPierdeConPiedra() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, lagarto.comparar(piedra));
		assertEquals("lagarto pierde contra piedra", lagarto.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararLagartoEmpataConLagarto() {
		//TODO para mis queridos alumnos testCompararTijeraEmpataConTiera 
		assertEquals(0, lagarto.comparar(lagarto));
		assertEquals("lagarto empata con lagarto", lagarto.getDescripcionResultado()
													  .toLowerCase());
	}	
	
	//Casos de SPOCK
	@Test
	void testCompararSpockGanaATijera() {
		//TODO para mis queridos alumnos testCompararTijeraGanaAPapel() 
		assertEquals(1, spock.comparar(tijera));
		assertEquals("spock le gana a tijera", spock.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararSpockGanaATPiedra() {
		//TODO para mis queridos alumnos testCompararTijeraGanaAPapel() 
		assertEquals(1, spock.comparar(piedra));
		assertEquals("spock le gana a piedra", spock.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararSpockPierdeConPapel() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, spock.comparar(papel));
		assertEquals("spock pierde contra papel", spock.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararSpockPierdeConLagarto() {
		//TODO para mis queridos alumnos testCompararTijeraPierdeConPiedra 
		assertEquals(-1, spock.comparar(lagarto));
		assertEquals("spock pierde contra lagarto", spock.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararSpockEmpataConSpock() {
		//TODO para mis queridos alumnos testCompararTijeraEmpataConTiera 
		assertEquals(0, spock.comparar(spock));
		assertEquals("spock empata con spock", spock.getDescripcionResultado()
													  .toLowerCase());
	}
	
}
