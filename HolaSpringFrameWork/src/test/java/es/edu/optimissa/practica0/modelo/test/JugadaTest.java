package es.edu.optimissa.practica0.modelo.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.edu.alten.practica0.modelo.Jugada;
import es.edu.alten.practica0.modelo.Jugador;
import es.edu.alten.practica0.modelo.Piedra;
import es.edu.alten.practica0.modelo.PiedraPapelTijeraFactory;
import es.edu.alten.practica0.modelo.Spock;

class JugadaTest {
	Jugada jugada = null;

	@BeforeEach
	void setUp() throws Exception {
		Jugador jug1 = new Jugador(1,"Yelitza","Yeli", new Piedra());
		Jugador jug2 = new Jugador(2,"ordenador","pc", new Spock());
		jugada = new Jugada(1, new Date(), jug1, jug2);
	}

	@AfterEach
	void tearDown() throws Exception {
		jugada = null;
	}

	@Test
	void testGetDescripcionResultado() {
		assertEquals("piedra pierde contra spock", jugada.getDescripcionResultado());
	}

}
