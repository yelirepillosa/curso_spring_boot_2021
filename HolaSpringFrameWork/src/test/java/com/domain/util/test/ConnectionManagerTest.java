package com.domain.util.test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.domain.util.ConnectionManager;

class ConnectionManagerTest {
	
	Connection con;

	@BeforeEach
	void setUp() throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/alten", "root", "root");
		ConnectionManager.conectar();
	}

	@AfterEach
	void tearDown() throws Exception {
		con = null;
		ConnectionManager.closed();
	}

	@Test
	void testConectar() {
		try {
			ConnectionManager.conectar();
			assertTrue(true);
			
		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	void testClosed() {
		try {
			ConnectionManager.closed();
			assertTrue(true);
		} catch (SQLException e) {
			assertFalse(false);
			e.printStackTrace();
		}
	}
	
	void testGetConnection() {
		assertNotNull(ConnectionManager.getConnection());
	}

}
