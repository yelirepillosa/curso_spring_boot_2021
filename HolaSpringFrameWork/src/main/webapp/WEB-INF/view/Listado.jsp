<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="ISO-8859-1">

<title>${titulo}</title>
<h1>PROFESOR ${profesor}</h1>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
        
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Codigo</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Repositorio</th>
      </tr>
    </thead>
    <tbody>
    
    <c:forEach items="${alumnos}" var="alumno"> 
        <tr>
        <td class="container"> ${alumno.getCodigo()} </td>
        <td class="container"> ${alumno.getNombre()} </td>
        <td class="container"> ${alumno.getApellido()} </td>
        <td class="container"> ${alumno.getLinkArepositorio()} </td>
      </tr>

	</c:forEach> 
    
   
    </tbody>
  </table>
</div>
</body>
</html>