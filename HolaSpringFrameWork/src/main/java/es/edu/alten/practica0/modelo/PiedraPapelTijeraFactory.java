package es.edu.alten.practica0.modelo;

import java.util.ArrayList;
import java.util.List;

public abstract class PiedraPapelTijeraFactory {

	public final static int PIEDRA = 1;
	public final static int PAPEL = 2;
	public final static int TIJERA = 3;
	
	public final static int LAGARTO = 4;
	public final static int SPOCK	= 5;
	
	//atributos
	protected String descripcionResultado;
	private static List<PiedraPapelTijeraFactory> elementos;
	protected String nombre;
	protected int numero;
	
	//constructor de la clase
	public PiedraPapelTijeraFactory(String nombre, int numero) {
		this.nombre = nombre;
		this.numero = numero;
	}

	public String getDescripcionResultado() {
		return descripcionResultado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	//metodos de negocio
	public abstract boolean isMe(int pNum);
	public abstract int comparar(PiedraPapelTijeraFactory pipati);
	
	public static PiedraPapelTijeraFactory getInstance(int pNumero) {
		///el coraz�n del Fractory
		// 1ro el padre reconoce a todos sus hijos
		elementos = new ArrayList<PiedraPapelTijeraFactory>();
		elementos.add(new Piedra());
		elementos.add(new Papel());
		elementos.add(new Tijera());
		
		elementos.add(new Lagarto());
		elementos.add(new Spock());
		
		//este codigo siempre sera el mismo
		for (PiedraPapelTijeraFactory piedraPepelTijeraFactory : elementos) {
			if(piedraPepelTijeraFactory.isMe(pNumero))
			return piedraPepelTijeraFactory;
		}		
		
		return null;
	}
	
	
}
