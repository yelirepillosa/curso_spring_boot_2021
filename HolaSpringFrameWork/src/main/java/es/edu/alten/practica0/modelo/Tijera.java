package es.edu.alten.practica0.modelo;

public class Tijera extends PiedraPapelTijeraFactory{
	
	public Tijera() {
		this("Tijera", TIJERA);
	}
	public Tijera(String pNom, int PNum) {
		super(pNom, PNum);
	}
	
	@Override
	public boolean isMe(int pNum) {
		return pNum == TIJERA;
	}
	
	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		case PAPEL:
		case LAGARTO:
			resul=1;
			this.descripcionResultado = "tijera le gana a " + pPiedPapelTijera.getNombre();
			break;
			
        case PIEDRA:
        case SPOCK:
			resul=-1;
			this.descripcionResultado = "tijera pierde contra " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul=0;
			this.descripcionResultado = "tijera empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}
}
