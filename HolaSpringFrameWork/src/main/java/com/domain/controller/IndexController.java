package com.domain.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.domain.modelo.Alumno;
import com.domain.modelo.Modelo;
import com.domain.modelo.dao.AlumnoDao;
import es.edu.alten.practica0.modelo.*;

@Controller
public class IndexController {

	PiedraPapelTijeraFactory elementos;
	PiedraPapelTijeraFactory piedra, papel, tijera, lagarto, spock;
	
	@RequestMapping("/home")
	public String goIndex() {
		return "Index";
	}
	
	@RequestMapping("/")
	public String goPresentacion() {
		return "Presentacion";
	}
	
	/*@RequestMapping("/listado")
	public String goListado(Model model) {
		
		List<String> alumnos = new ArrayList<String>();
		alumnos.add("Juan");
		alumnos.add("Pedro");
		alumnos.add("Jos�");
		model.addAttribute("titulo", "Listado de alumnos");
		model.addAttribute("profesor", "Gabriel Casas");
		model.addAttribute("alumnos", alumnos);

	return "Listado";
	}*/
	
	@RequestMapping("/listado")
	public String goListado(Model model) {
		
		List<Modelo> alumnos = new ArrayList<Modelo>();
		AlumnoDao alu= new AlumnoDao();
		try {
			alumnos=alu.leer(new Alumno());
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("titulo", "Listado de alumnos");
		model.addAttribute("profesor", "Gabriel Casas");
		model.addAttribute("alumnos",alumnos);

	return "Listado";
	}
	
	//M�todo Piedra, Papel tijera, Lagarto y Spock
	
	@RequestMapping("/juego")
	public String goPiedraPapelTijeraLagartoSpock(Model  model) {
		
		List<PiedraPapelTijeraFactory> opciones = new ArrayList<PiedraPapelTijeraFactory>();
		List<Modelo> alumnos = new ArrayList<Modelo>();
		AlumnoDao alu= new AlumnoDao();
		try {
			alumnos=alu.leer(new Alumno());
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for(int i =1;i<6;i++)
			opciones.add(PiedraPapelTijeraFactory.getInstance(i));
		
		model.addAttribute("titulo", "Piedra, Papel, Tijera, Lagarto y Spock");
		model.addAttribute("opciones", opciones);
		model.addAttribute("alumnos",alumnos);
		
	return "PiedraPapelTijeraLagartoSpock";
	}

	
	
	//Recibe el valor enviado en la pagina
	@RequestMapping("/resolverJuego")
	public String goMostrarResultado(@RequestParam(required = false) Integer selOpcion, String selNombre, Model model) {
		
		//Elecci�n automatica del ordenador 
		PiedraPapelTijeraFactory ordenador = PiedraPapelTijeraFactory.getInstance((int)(Math.random()*100%5+1));
		
		//Parametro que se selecciona en el JSP
		PiedraPapelTijeraFactory jugador = PiedraPapelTijeraFactory.getInstance(selOpcion.intValue());
		
		jugador.comparar(ordenador);
		
		model.addAttribute("nombre", selNombre.toString());
		model.addAttribute("jugador", jugador);
		model.addAttribute("ordenador", ordenador);
		model.addAttribute("resultado", jugador.getDescripcionResultado());
		
	return "MostrarResultado";
	}
}
