package com.domain.modelo.dao;

import java.sql.SQLException;
import java.util.List;

import com.domain.modelo.Modelo;



public interface DAO {
	
	public void agregar(Modelo pModel) throws ClassNotFoundException, SQLException;
	public void modificar(Modelo pModel) throws ClassNotFoundException, SQLException;
	public void eliminar(Modelo pModel) throws ClassNotFoundException, SQLException;
	public List<Modelo>leer(Modelo pModel) throws ClassNotFoundException, SQLException;

}
