package com.domain.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import com.domain.modelo.Alumno;
import com.domain.modelo.Modelo;
import com.domain.util.ConnectionManager;

public class AlumnoDao implements DAO {

	public AlumnoDao() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void agregar(Modelo pModel) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConnection();

		String sql = new String("insert into alumnos(alu_nombre, alu_apellido, alu_conocimientos,alu_git) values(?,?,?,? )");

		Alumno alu =(Alumno) pModel;
		PreparedStatement stm = con.prepareStatement(sql);
		stm.setString(1, alu.getNombre());
		stm.setString(2, alu.getApellido());
		stm.setString(3, alu.getEstudios());
		stm.setString(4, alu.getLinkArepositorio());
		stm.execute();

	}

	@Override
	public void modificar(Modelo pModel) throws ClassNotFoundException, SQLException{
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConnection();

		String sql = new String("update alumnos set alu_nombre=?,alu_apellido=?,alu_conocimientos=?,alu_git=? where alu_id=?");

		Alumno alu =(Alumno) pModel;
		PreparedStatement stm = con.prepareStatement(sql);
		stm.setString(1, alu.getNombre());
		stm.setString(2, alu.getApellido());
		stm.setString(3, alu.getEstudios());
		stm.setString(4, alu.getLinkArepositorio());
		stm.setInt(5, alu.getCodigo());
		stm.execute();
	}

	@Override
	public void eliminar(Modelo pModel) throws ClassNotFoundException, SQLException{
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConnection();

		String sql = new String("delete from alumnos where alu_id=?");

		Alumno alu =(Alumno) pModel;
		PreparedStatement stm = con.prepareStatement(sql);
		stm.setInt(1, alu.getCodigo());
		stm.execute();

	}

	@Override
	public List<Modelo> leer(Modelo pModel) throws ClassNotFoundException, SQLException{
		List<Modelo> lista = new ArrayList<Modelo>();
		ConnectionManager.conectar();
		Connection conn = ConnectionManager.getConnection();
		StringBuilder sql = new StringBuilder("select * from alumnos");

		Alumno alumno = (Alumno) pModel;
		if(alumno.getCodigo()>0) {
		sql.append(" where alu_id=?");
		}

		PreparedStatement stm = conn.prepareStatement(sql.toString());

		if(alumno.getCodigo()>0) {
		stm.setInt(1, alumno.getCodigo());
		}

		ResultSet rs = stm.executeQuery();

		while (rs.next()) {
	    alumno = new Alumno();
	    alumno.setCodigo(rs.getInt("alu_ID"));
		alumno.setNombre(rs.getString("alu_nombre"));
		alumno.setApellido(rs.getString("alu_apellido"));
		alumno.setEstudios(rs.getString("alu_conocimientos"));
		alumno.setLinkArepositorio(rs.getString("alu_git"));
		lista.add(alumno);
		}
		return lista;
		}
}
