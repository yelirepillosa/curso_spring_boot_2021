package com.yeliCode.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yeliCode.model.Persona;

public interface IPersonaRepo extends JpaRepository<Persona, Integer> {

}
